package com.example.demo;

import java.util.ArrayList;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainArisan {
	static Scanner input = new Scanner(System.in);
	static ArrayList<Arisan> arisan = new ArrayList<>();
	static Arisan ar = new Arisan();
	static ArrayList<Arisan> pemenang = new ArrayList<>();
	static boolean loops = true;
	static int uangArisan = 150_000;
	static int totalUang;

	public static void main(String[] args) {
//		SpringApplication.run(Arisan.class, args);
		menu();
		
	}
	public static void menu() {
		while (loops) {
			System.out.println("Arisan");
			System.out.println("=============");
			System.out.println("1. Tambah Anggota");
			System.out.println("2. Kocok Pemenang");
			System.out.println("3. Exit");
			System.out.println("=============");
			System.out.print("Pilih menu : ");
			int pil = Integer.parseInt(input.nextLine());
			if (pil == 1) {
				tambahAnggota();
			} else if (pil == 2) {
				kocokPemenang();
			} else {
				System.out.println("Keluar dari Aplikasi");
				System.exit(0);
			}
		}
	}
	public static void tambahAnggota() {
		System.out.print("Masukan nama : ");
		String name = input.nextLine();
		System.out.print("Masukan kode : ");
		int code = Integer.parseInt(input.nextLine());
		ar = new Arisan(name, code);
		arisan.add(ar);
		totalUang = uangArisan * (arisan.size()+pemenang.size());	
	}
	public static void kocokPemenang() {
		if (arisan.size()==0) {
			System.out.println("Belum ada anggota arisan, silakan tambah terlebih dahulu");
		} else {
			int rand = (int) (Math.random() * arisan.size())+1;
			for (int i = 0; i < arisan.size(); i++) {
				if (arisan.size()<=1) {
					System.out.println("Hasil Kocokan : ");
					System.out.println("Nama : " + arisan.get(i).getName());
					System.out.println("Mendapatkan uang : " + totalUang);
					pemenang.add(arisan.get(0));
					arisan.remove(0);
				} else {
						if (rand == arisan.get(i).getKode()) {
							System.out.println("Hasil Kocokan : ");
							System.out.println("Nama : " + arisan.get(i).getName());
							System.out.println("Mendapatkan uang : " + totalUang);
							pemenang.add(arisan.get(i));
							arisan.remove(i);
							break;
						}
				}
			}
		}
		// for (int i = 0; i < pemenang.size(); i++) {
		// 	System.out.println(pemenang.get(i).getName());
		// }
	}

}
