package com.example.demo;

public class Arisan {
    private String name;
    private int kode;

    public Arisan(String name, int kode) {
        this.name = name;
        this.kode = kode;
    }
    public Arisan() {
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getKode() {
        return kode;
    }
    public void setKode(int kode) {
        this.kode = kode;
    }
    
}
